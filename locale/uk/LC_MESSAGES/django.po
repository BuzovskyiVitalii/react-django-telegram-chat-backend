# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-08-30 19:31+0300\n"
"PO-Revision-Date: 2021-08-18 15:07+0000\n"
"Last-Translator: b'  <buzovskiy.v@gmail.com>'\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=(n % 1 == 0 && n % 10 == 1 && n % 100 != "
"11 ? 0 : n % 1 == 0 && n % 10 >= 2 && n % 10 <= 4 && (n % 100 < 12 || n % "
"100 > 14) ? 1 : n % 1 == 0 && (n % 10 ==0 || (n % 10 >=5 && n % 10 <=9) || "
"(n % 100 >=11 && n % 100 <=14 )) ? 2: 3);\n"
"X-Translated-Using: django-rosetta 0.9.6\n"

#: .\chat\management\commands\telegram_listener.py:136
msgid ""
"In order to become a chat operator for website *{}* enter passcode.\n"
"Enter /cancel to quit"
msgstr ""

#: .\chat\management\commands\telegram_listener.py:140
#: .\chat\management\commands\telegram_listener.py:184
#: .\chat\management\commands\telegram_listener.py:216
#: .\chat\management\commands\telegram_listener.py:230
#: .\chat\management\commands\telegram_listener.py:339
#: .\chat\management\commands\telegram_listener.py:358
msgid "<< Return to previous menu"
msgstr ""

#: .\chat\management\commands\telegram_listener.py:188
msgid ""
"You successfully removed website *{}* from your chat!\n"
"Enter /cancel to quit or press return to previous menu"
msgstr ""

#: .\chat\management\commands\telegram_listener.py:221
msgid ""
"You are successfully registered as a chat manager for website *{}*!\n"
"Enter /cancel to quit or press return to previous menu"
msgstr ""

#: .\chat\management\commands\telegram_listener.py:234
msgid ""
"You entered incorrect passcode for website *{}*. Please repeat or send /"
"cancel to stop the registration process"
msgstr ""

#: .\chat\management\commands\telegram_listener.py:266
msgid ""
"Bye!\n"
"You quited.\n"
"In order to start again enter */manager* or */start*"
msgstr ""

#: .\chat\management\commands\telegram_listener.py:309
msgid "Choose action or enter /cancel to quit:"
msgstr ""

#: .\chat\management\commands\telegram_listener.py:312
msgid "Become a chat operator"
msgstr ""

#: .\chat\management\commands\telegram_listener.py:315
msgid "Stop being a chat operator"
msgstr ""

#: .\chat\management\commands\telegram_listener.py:323
msgid ""
"You are missing from our database. In order to use this telegram bot please "
"enter the passcode or send /cancel to stop the registration process."
msgstr ""

#: .\chat\management\commands\telegram_listener.py:344
msgid "Choose a domain or send /cancel to quit"
msgstr ""

#: .\chat\management\commands\telegram_listener.py:363
msgid ""
"Select a website that you want to remove from your chat or send /cancel to "
"quit"
msgstr ""

#: .\chat\words.py:4
msgid "Chat"
msgstr "Чат"

#: .\chat\words.py:5
msgid "Type message"
msgstr "Напишіть повідомлення"

#: .\chat\words.py:6
msgid "Write your question or comment and we will answer right away!"
msgstr "Напишіть Ваше запитання або коментар, і ми тут же відповімо!"

#: .\chat\words.py:7
msgid "You"
msgstr "Ви"

#: .\project\settings.py:157 .\project\settings.py:171
msgid "English"
msgstr ""

#: .\project\settings.py:158
msgid "Polish"
msgstr ""

#: .\project\settings.py:159 .\project\settings.py:172
msgid "Russian"
msgstr ""

#: .\project\settings.py:160
msgid "Czech"
msgstr ""

#: .\project\settings.py:161
msgid "German"
msgstr ""

#: .\project\settings.py:162
msgid "Lithuanian"
msgstr ""

#: .\project\settings.py:163
msgid "Greek"
msgstr ""

#: .\project\settings.py:164
msgid "hungarian"
msgstr ""

#: .\project\settings.py:165
msgid "Ukrainian"
msgstr ""

#~ msgid "chat"
#~ msgstr "Чат"
