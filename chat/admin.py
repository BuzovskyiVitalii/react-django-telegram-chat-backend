from django.contrib import admin
from .models import Manager, Shop

# Register your models here.


# class ManagersInline(admin.TabularInline):
#     model = Manager
#     extra = 1
#     # readonly_fields = ("get_image",)

class ManagerShopInline(admin.TabularInline):
    model = Manager.shop.through
    extra = 0


@admin.register(Manager)
class ManagerAdmin(admin.ModelAdmin):
    """Chat managers"""
    pass


@admin.register(Shop)
class ShopAdmin(admin.ModelAdmin):
    """Shops"""
    list_display = ('shop', 'passcode')
    inlines = [ManagerShopInline]

# admin.site.register(Shop)

