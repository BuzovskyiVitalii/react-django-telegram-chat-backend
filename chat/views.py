from django.http import HttpResponse
from django.conf import settings
from rest_framework.parsers import JSONParser

from rest_framework.response import Response
from rest_framework.views import APIView

from datetime import datetime

import json
from pprint import PrettyPrinter

import os

from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework import parsers

from django.utils import translation

import time
from django.conf import settings
from django.shortcuts import render


class GetTranslations(APIView):
    """
    api/v1/chat/get-translations
    """

    def post(self, request):
        language_code = request.data['params']['language_code']
        translation.activate(language_code)
        from .words import words
        return Response(words)
