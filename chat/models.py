from django.db import models
from django.db.models import ObjectDoesNotExist


class Shop(models.Model):
    """Shops. Domains where chat are used"""
    shop = models.CharField("Shop", max_length=250, unique=True)
    passcode = models.CharField("Passcode", max_length=250, null=True)
    objects = models.Manager()

    def __str__(self):
        return str(self.shop)

    class Meta:
        verbose_name = "Shop"
        verbose_name_plural = "Shops"


class Manager(models.Model):
    """Managers of a chat"""
    first_name = models.CharField("First name", max_length=250, blank=True)
    last_name = models.CharField("Last name", max_length=250)
    chat_id = models.CharField("Manager chat id", max_length=250, unique=True)
    shop = models.ManyToManyField(Shop, verbose_name="Shops")

    objects = models.Manager()

    def __str__(self):
        return str(self.first_name) + ' ' + str(self.last_name)

    class Meta:
        verbose_name = "Manager of a chat"
        verbose_name_plural = "Managers of a chat"

    @classmethod
    def get_chat_ids_list(cls, domain):
        """
        Function that returns a list of telegram chat ids bound to specific domain
        :param domain: (string) domain
        :return: (list) the list of telegram chat ids
        """
        try:
            shop = Shop.objects.filter(shop=domain).get()
        except ObjectDoesNotExist:
            return []

        managers = map(lambda item: item.chat_id, cls.objects.filter(shop=shop).all())
        return list(managers)
