# Generated by Django 3.1.5 on 2021-07-21 18:22

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('chat', '0004_auto_20210720_1858'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='manager',
            options={'verbose_name': 'Manager of a chat', 'verbose_name_plural': 'Managers of a chat'},
        ),
    ]
