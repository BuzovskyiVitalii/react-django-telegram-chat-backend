from channels.generic.websocket import WebsocketConsumer
import json
from urllib.parse import parse_qs
import time
from time import sleep
from asgiref.sync import async_to_sync

import redis

import telegram
from telegram import Bot, Update, KeyboardButton, ReplyKeyboardRemove, ReplyKeyboardMarkup
from telegram.ext import CallbackContext, Updater, Filters, MessageHandler
from telegram.utils.request import Request

from django.conf import settings
from django.templatetags.static import static
# Импортируем класс Manager, чтобы получить список chat id пользователей телеграм
from .models import Manager, Shop
from .words import words
from django.utils import translation


redis_client = redis.Redis(**settings.REDIS['channel'])


class ChatConsumer(WebsocketConsumer):

    @property
    def fields(self):
        return {
            'message': {'default': str(words['greeting'])},
            'sender': {'default': 'manager'},
            'site_domain': {'default': ''},
            'name': {'default': 'Weestep'},
            'surname': {'default': ''},
            'email': {'default': ''},
            'chat_ids': {'default': ''},
            'unix_time': {'default': int(time.time()*1000)},
            'image_src': {'default': settings.MY_BASE_URL + static('chat/weestep-chat-logo.jpg')},
        }

    language_code = settings.LANGUAGE_CODE

    def connect(self):
        """
            https://redis.io/topics/data-types-intro
            В бд redis создается ключ asgi:group:chat_%
            В redis-cli: KEYS *
            Добавление значения zadd hackers 1940 "Alan Kay"
            Показать значения zrange hackers 0 -1 withscores
            В него записываются сообщения.
            Запуск redis через VB: redis-server --protected-mode no
        """

        params = parse_qs(self.scope['query_string'].decode('utf8'))
        if 'language_code' in params and len(params['language_code']) and params['language_code']:
            self.language_code = params['language_code'][0]
        translation.activate(self.language_code)

        # Obtains the 'room_name' parameter from the URL route that opened
        # the WebSocket connection to the consumer.
        self.room_name = self.scope['url_route']['kwargs']['room_name']
        self.room_group_name = 'chat_%s' % self.room_name

        # Join room group
        async_to_sync(self.channel_layer.group_add)(
            self.room_group_name,
            self.channel_name
        )

        self.accept()

        chat_messages_history_list = redis_client.lrange(self.room_group_name, 0, -1)
        chat_messages_history_list = list(map(lambda item: json.loads(item), chat_messages_history_list))
        # Если есть история сообщений, отправляем ее
        if len(chat_messages_history_list):
            server_messages_list = chat_messages_history_list
        else:
            message = {}
            for field, _settings in self.fields.items():
                message[field] = _settings['default']
            server_messages_list = [message]

        async_to_sync(self.channel_layer.group_send)(
            self.room_group_name,
            {
                'type': "chat.message",
                'serverMessagesList': server_messages_list,
            }
        )

    def disconnect(self, close_code):
        # Leave room group
        async_to_sync(self.channel_layer.group_discard)(
            self.room_group_name,
            self.channel_name
        )

    # Receive message from WebSocket
    def receive(self, text_data):
        message_data_dict = json.loads(text_data)
        if len(message_data_dict['message']) > 1000:
            return False

        if not message_data_dict['site_domain']:
            # If no information about domain of a client
            return False

        chat_ids = Manager.get_chat_ids_list(domain=message_data_dict['site_domain'])
        if not len(chat_ids):
            return False

        # Send message to room group
        async_to_sync(self.channel_layer.group_send)(
            self.room_group_name, {
                'type': "chat.message",
                'serverMessagesList': [message_data_dict],
            }
        )

        bot = Bot(token=settings.MY_CHAT_BOT_TOKEN)
        id_message_bot_list = []
        for chat_id in chat_ids:
            try:
                message_id = bot.sendMessage(
                    chat_id=chat_id,
                    text=f"*{message_data_dict['site_domain']}:*\n{message_data_dict['message']}",
                    parse_mode='markdown'
                ).message_id
                id_message_bot_list.append(message_id)
            except (telegram.error.Unauthorized, telegram.error.BadRequest):
                # Manager.objects.filter(chat_id=chat_id).delete()  # Delete chat manager from db
                pass
            except Exception as e:
                print(e)

        for idMessageBot in id_message_bot_list:
            # Сопоставляем id сообщения, присвоенное ботом, с названием чата
            redis_client.hset(
                "chat:idMessageBot:roomGroupName",
                mapping={str(idMessageBot): self.room_group_name}
            )
            redis_client.expire("chat:idMessageBot:roomGroupName", 60 * 60 * 24 * 30 * 6)

        message_data_dict['chat_ids'] = chat_ids

        record_message_to_redis(self.room_group_name, **message_data_dict)

        # async_to_sync(self.channel_layer.send)(str(message_id), {
        #     'room_group_name': self.room_group_name,
        # })

    # Receive message from room group
    def chat_message(self, event):
        # Send message to WebSocket
        self.send(text_data=json.dumps({
            'serverMessagesList': event['serverMessagesList']
        }))


def record_message_to_redis(room_group_name, **kwargs):
    json_object = json.dumps(kwargs)
    redis_client.rpush(room_group_name, json_object)
    redis_client.expire(room_group_name, 60*60*24*30*6)

