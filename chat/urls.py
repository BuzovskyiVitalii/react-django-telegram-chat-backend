from django.urls import path

from . import views


urlpatterns = [
    path('get-translations/', views.GetTranslations.as_view()),
]
