from django.utils.translation import gettext_lazy as _

words = {
    'chat': _('Chat'),
    'type_message_placeholder': _('Type message'),
    'greeting': _('Write your question or comment and we will answer right away!'),
    'you': _('You'),
}
