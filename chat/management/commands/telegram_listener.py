from django.core.management.base import BaseCommand
import time
import redis
from telegram import Bot, Update, ReplyKeyboardRemove, InlineKeyboardButton, \
    InlineKeyboardMarkup
from telegram.ext import CallbackContext, Updater, Filters, MessageHandler, ConversationHandler, CommandHandler, \
    CallbackQueryHandler
from telegram.utils.request import Request
from channels.layers import get_channel_layer
from asgiref.sync import async_to_sync
from django.conf import settings
from django.db.models import ObjectDoesNotExist
from django.utils import translation
from django.utils.translation import gettext_lazy as _
from chat.consumers import record_message_to_redis
from chat.consumers import ChatConsumer
from chat.models import Manager, Shop

redis_client = redis.Redis(**settings.REDIS['channel'])


class Command(BaseCommand):
    help = 'Displays current time'

    def handle(self, *args, **kwargs):
        req = Request(
            connect_timeout=5,
        )
        bot = Bot(
            request=req,
            token=settings.MY_CHAT_BOT_TOKEN,
        )
        updater = Updater(
            bot=bot,
            use_context=True,
        )
        # print(updater.bot.get_me())
        updater.dispatcher.add_handler(conv_handler)
        updater.dispatcher.add_handler(MessageHandler(filters=Filters.text, callback=message_handler))

        updater.start_polling()
        updater.idle()


def message_handler(update: Update, context: CallbackContext):
    """Обработка сообщения, которое пишет пользователь в телеграм бот"""
    channel_layer = get_channel_layer()

    if not (hasattr(update, 'message') and hasattr(update.message.reply_to_message, 'message_id')):
        return False

    message_id = update.message.reply_to_message.message_id
    room_group_name = redis_client.hget('chat:idMessageBot:roomGroupName', message_id)
    if room_group_name is None:
        return False

    message_data_dict = {}
    for field, fieldSettings in ChatConsumer().fields.items():
        message_data_dict[field] = fieldSettings['default']
    message_data_dict['message'] = update.message.text
    if hasattr(update.message.chat, 'first_name') and update.message.chat.first_name:
        message_data_dict['name'] = update.message.chat.first_name
    if hasattr(update.message.chat, 'last_name') and update.message.chat.last_name:
        message_data_dict['surname'] = update.message.chat.last_name
    message_data_dict['chat_ids'] = [update.message.chat.id]
    message_data_dict['unix_time'] = int(time.time() * 1000)
    # Информацию о сообщении из telegram запоминаем в redis
    record_message_to_redis(room_group_name, **message_data_dict)

    # Send message to room group
    async_to_sync(channel_layer.group_send)(
        room_group_name, {
            'type': "chat.message",
            'serverMessagesList': [message_data_dict],
        }
    )


[SET_LANGUAGE, PASS_CODE, BIND_SHOP_TO_MANAGER, UNBIND_SHOP_FROM_MANAGER, MAIN_MENU, CHECK_PASS_CODE_FOR_SHOP,
 ACTION_SUCCESS] = range(7)


def become_chat_manager(update: Update, context: CallbackContext) -> int:
    """Starts the conversation and asks the user about their gender."""

    update.message.reply_text(
        'Hi! To become a chat manager, enter the passcode.\n'
        'Send /cancel to stop the registration process.',
    )

    return PASS_CODE


def pass_code(update: Update, context: CallbackContext) -> int:
    """Check if passcode is correct and saves telegram user to the database as a chat manager"""
    if len(Shop.objects.filter(passcode=update.message.text).all()):

        first_name = update.message.chat.first_name if update.message.chat.first_name else ''
        last_name = update.message.chat.last_name if update.message.chat.last_name else 'Operator'

        manager, created = Manager.objects.update_or_create(
            chat_id=update.message.chat.id,
            defaults={'first_name': first_name, 'last_name': last_name, 'chat_id': update.message.chat.id},
        )
        return show_main_menu(update=update, context=context)
    else:
        update.message.reply_text(
            'You entered incorrect passcode. Please repeat or send /cancel to stop the registration process. ',
        )
        return PASS_CODE


def bind_shop(update: Update, context: CallbackContext) -> int:
    """Function that handles the event of shop choosing"""
    query = update.callback_query
    query.answer()
    context.user_data['domain_to_bind'] = query.data
    reply_text = str(_("In order to become a chat operator for website *{}* enter passcode.\n"
                       "Enter /cancel to quit")).format(query.data)
    keyboard = [
        [
            InlineKeyboardButton(str(_("<< Return to previous menu")), callback_data='return_to_previous_menu'),
        ]
    ]
    reply_markup = InlineKeyboardMarkup(keyboard)
    query.edit_message_text(
        text=reply_text, reply_markup=reply_markup, parse_mode='markdown'
    )

    return CHECK_PASS_CODE_FOR_SHOP


def unbind_shop(update: Update, context: CallbackContext) -> int:
    """Function that handles the event of shop choosing"""
    query = update.callback_query
    query.answer()
    manager = Manager.objects.filter(chat_id=query.from_user.id).get()
    shop = Shop.objects.filter(shop=query.data).get()
    manager.shop.remove(shop)
    keyboard = [
        [
            InlineKeyboardButton(str(_("<< Return to previous menu")), callback_data='return_to_previous_menu'),
        ]
    ]
    reply_markup = InlineKeyboardMarkup(keyboard)
    reply_text = str(_("You successfully removed website *{}* from your chat!\nEnter /cancel to quit "
                       "or press return to previous menu")).format(query.data)
    query.edit_message_text(
        text=reply_text, reply_markup=reply_markup, parse_mode='markdown'
    )
    return ACTION_SUCCESS


def check_pass_code_for_shop(update: Update, context: CallbackContext) -> int:
    """
    The function where the passcode is checked and manager is added to shop
    :param update:
    :param context: the name of the domain is passed through key domain_to_bind of user_data attribute
    :return: (integer) return CHECK_PASS_CODE_FOR_SHOP if check fails and ACTION_SUCCESS if check success
    """
    shop = Shop.objects.filter(shop=context.user_data['domain_to_bind'], passcode=update.message.text).all()
    if len(shop):
        chat_id = update.message.chat.id
        try:
            manager = Manager.objects.filter(chat_id=chat_id).get()
            shop = Shop.objects.filter(shop=context.user_data['domain_to_bind']).get()
            manager.shop.add(shop)
        except ObjectDoesNotExist as e:
            update.message.reply_text(
                text=f"""*Error*: {e}\nBye!""", parse_mode='markdown'
            )
            return ConversationHandler.END
        keyboard = [
            [
                InlineKeyboardButton(str(_("<< Return to previous menu")), callback_data='return_to_previous_menu'),
            ]
        ]
        reply_markup = InlineKeyboardMarkup(keyboard)

        reply_text = str(_("You are successfully registered as a chat manager for website *{}*!\nEnter /cancel to quit "
                           "or press return to previous menu")).format(context.user_data['domain_to_bind'])
        update.message.reply_text(
            text=reply_text, reply_markup=reply_markup, parse_mode='markdown'
        )
        return ACTION_SUCCESS
    else:
        keyboard = [
            [
                InlineKeyboardButton(str(_("<< Return to previous menu")), callback_data='return_to_previous_menu'),
            ]
        ]
        reply_markup = InlineKeyboardMarkup(keyboard)
        reply_text = str(_("You entered incorrect passcode for website *{}*. Please repeat or send /cancel to stop "
                           "the registration process")).format(context.user_data['domain_to_bind'])
        update.message.reply_text(
            text=reply_text, reply_markup=reply_markup, parse_mode='markdown'
        )
        return CHECK_PASS_CODE_FOR_SHOP


def cancel(update: Update, context: CallbackContext) -> int:
    """Cancels and ends the conversation."""
    reply_text = str(_("Bye!\nYou quited.\nIn order to start again enter */manager* or */start*"))
    update.message.reply_text(
        reply_text,
        reply_markup=ReplyKeyboardRemove(),
        parse_mode='markdown',
    )

    return ConversationHandler.END


def show_languages_buttons(update: Update, context: CallbackContext):
    """Show list of languages"""

    languages_for_keyboard = [(settings.LANGUAGE_CODE, settings.LANGUAGE_CODE)]
    if hasattr(settings, 'LANGUAGES_TELEGRAM_BOT'):
        languages_for_keyboard = list(settings.LANGUAGES_TELEGRAM_BOT)
    elif hasattr(settings, 'LANGUAGES'):
        languages_for_keyboard = list(settings.LANGUAGES)

    keyboard = generate_inline_keyboard(items=languages_for_keyboard, columns=2)
    reply_markup = InlineKeyboardMarkup(keyboard)
    update.message.reply_text('Hi!\nPlease choose language.\nOr enter /cancel to quit', reply_markup=reply_markup)

    return SET_LANGUAGE


def set_language(update: Update, context: CallbackContext):
    query = update.callback_query
    query.answer()
    translation.activate(query.data)
    return show_main_menu(update=update, context=context)


def show_main_menu(update: Update, context: CallbackContext):
    query = None
    try:
        query = update.callback_query
        query.answer()
        chat_id = query.from_user.id
    except AttributeError:
        chat_id = update.message.chat.id
    manager_query_set = Manager.objects.filter(chat_id=chat_id)
    if len(manager_query_set):
        reply_text = str(_("Choose action or enter /cancel to quit:"))
        keyboard = []
        if len(Shop.objects.exclude(manager=manager_query_set.get())):
            keyboard.append([InlineKeyboardButton(str(_("Become a chat operator")),
                                                  callback_data='show_shops_to_bind')])
        if len(Shop.objects.filter(manager=manager_query_set.get())):
            keyboard.append([InlineKeyboardButton(str(_("Stop being a chat operator")),
                                                  callback_data='show_shops_to_unbind')])
        try:
            query.edit_message_text(text=str(reply_text), reply_markup=InlineKeyboardMarkup(keyboard))
        except AttributeError:
            update.message.reply_text(text=str(reply_text), reply_markup=InlineKeyboardMarkup(keyboard))

    else:
        reply_text = _("You are missing from our database. In order to use this telegram bot please enter the "
                       "passcode or send /cancel to stop the registration process.")
        query.edit_message_text(text=str(reply_text))
        return PASS_CODE

    return MAIN_MENU


def show_shops_to_bind(update: Update, context: CallbackContext):
    query = update.callback_query
    query.answer()
    chat_id = query.from_user.id
    manager = Manager.objects.filter(chat_id=chat_id).get()
    shops = Shop.objects.exclude(manager=manager)
    shops_for_keyboard = list(map(lambda shop: (shop.shop, shop.shop), shops))
    keyboard = generate_inline_keyboard(shops_for_keyboard, 2)
    keyboard.append([InlineKeyboardButton(str(_("<< Return to previous menu")),
                                          callback_data='return_to_previous_menu')])

    reply_markup = InlineKeyboardMarkup(keyboard)
    # Send message with text and appended InlineKeyboard
    reply_text = _("Choose a domain or send /cancel to quit")
    query.edit_message_text(text=str(reply_text), reply_markup=reply_markup)
    return BIND_SHOP_TO_MANAGER


def show_shops_to_unbind(update: Update, context: CallbackContext):
    """Function that show a list of shops to remove from manager"""
    query = update.callback_query
    query.answer()
    chat_id = query.from_user.id
    manager = Manager.objects.filter(chat_id=chat_id).get()
    shops = Shop.objects.filter(manager=manager)
    shops_for_keyboard = list(map(lambda shop: (shop.shop, shop.shop), shops))
    keyboard = generate_inline_keyboard(shops_for_keyboard, 2)
    keyboard.append([InlineKeyboardButton(str(_("<< Return to previous menu")),
                                          callback_data='return_to_previous_menu')])

    reply_markup = InlineKeyboardMarkup(keyboard)
    # Send message with text and appended InlineKeyboard
    reply_text = _("Select a website that you want to remove from your chat or send /cancel to quit")
    query.edit_message_text(text=str(reply_text), reply_markup=reply_markup)
    return UNBIND_SHOP_FROM_MANAGER


conv_handler = ConversationHandler(
    entry_points=[
        CommandHandler('start', show_languages_buttons),
        CommandHandler('manager', show_languages_buttons),
    ],
    states={
        SET_LANGUAGE: [
            CallbackQueryHandler(set_language),
            MessageHandler(Filters.text & ~Filters.command, cancel),
        ],
        MAIN_MENU: [
            MessageHandler(Filters.text & ~Filters.command, cancel),
            CallbackQueryHandler(show_shops_to_bind, pattern='^show_shops_to_bind$'),
            CallbackQueryHandler(show_shops_to_unbind, pattern='^show_shops_to_unbind$'),
        ],
        PASS_CODE: [MessageHandler(Filters.text & ~Filters.command, pass_code)],
        BIND_SHOP_TO_MANAGER: [
            MessageHandler(Filters.text & ~Filters.command, cancel),
            CallbackQueryHandler(show_main_menu, pattern='^return_to_previous_menu$'),
            CallbackQueryHandler(bind_shop),
        ],
        UNBIND_SHOP_FROM_MANAGER: [
            MessageHandler(Filters.text & ~Filters.command, cancel),
            CallbackQueryHandler(show_main_menu, pattern='^return_to_previous_menu$'),
            CallbackQueryHandler(unbind_shop),
        ],
        CHECK_PASS_CODE_FOR_SHOP: [
            MessageHandler(Filters.text & ~Filters.command, check_pass_code_for_shop),
            CallbackQueryHandler(show_main_menu, pattern='^return_to_previous_menu$'),
        ],
        ACTION_SUCCESS: [
            MessageHandler(Filters.text & ~Filters.command, cancel),
            CallbackQueryHandler(show_main_menu, pattern='^return_to_previous_menu$'),
        ]
    },
    fallbacks=[CommandHandler('cancel', cancel)],
)


def generate_inline_keyboard(items, columns):
    """
    A function that generates a list of buttons

    :param items: (list) a list of tuples. First element of a tuple is a value that is passed to callback_data,
    the second is a name of a button. Both first and second elements must be strings
    :param columns: (integer) a number of columns of a keyboard
    :return: (list) list of buttons
    """
    keyboard = [[]]
    for item in items:
        button = InlineKeyboardButton(str(item[1]), callback_data=str(item[0]))
        if len(keyboard[-1]) < columns:
            keyboard[-1].append(button)
        else:
            keyboard.append([])
            keyboard[-1].append(button)
    return keyboard
